Name:           fisher
Version:        4.4.5
Release:        1
Summary:        A plugin manager for Fish.
License:        MIT
URL:            https://github.com/jorgebucaran/fisher
Source0:        https://github.com/jorgebucaran/fisher/archive/refs/tags/%{version}.tar.gz
BuildArch:      noarch

Requires:       fish
Requires:       curl
Requires:       tar
Requires:       coreutils
BuildRequires:  fish
BuildRequires:  binutils

%description
Manage functions, completions, bindings, and snippets from the command line. Extend your shell capabilities, change the look of your prompt and create repeatable configurations across different systems effortlessly.

    100% pure-Fish—easy to contribute to or modify.
    Blazing fast concurrent plugin downloads.
    Zero configuration out of the box.
    Oh My Fish! plugin support.


%prep
%autosetup -p1 -n %{name}-%{version}

%install
install -vDm644 -t "%{buildroot}/%{_datarootdir}/fish/vendor_functions.d" functions/fisher.fish
install -vDm644 -t "%{buildroot}/%{_datarootdir}/fish/vendor_completions.d" completions/fisher.fish

# documentation
install -vDm644 -t "%{buildroot}/%{_docdir}/%{name}" README.md

# license
install -vDm644 -t "%{buildroot}/%{_datarootdir}/%{name}" LICENSE.md

%check
#tests need fishtape, which does not exists for now

%files 
%license LICENSE.md
%doc README.md
%{_datarootdir}/%{name}
%{_datarootdir}/fish/*

%changelog
* Sat Nov 9 2024 lichaoran <pkwarcraft@hotmail.com> - 4.4.5-1
- Update version to 4.4.5

* Fri Dec 15 2023 Vicoloa <lvkun@uniontech.com> - 4.4.4-1
- Update version to 4.4.4
- Fix: fix fishshell.com configuration link anchor and update docs to include theme instructions

* Thu Jun 15 2023 lichaoran <pkwarcraft@hotmail.com> - 4.4.3-2
- Fix: add valid license and remove incorrect provides

* Sun Feb 26 2023 lichaoran <pkwarcraft@hotmail.com> - 4.4.3-1
- Package init
